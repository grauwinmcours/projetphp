CREATE DATABASE ecommercecasquettes CHARACTER SET 'utf8';
USE ecommercecasquettes;

CREATE TABLE Clients
(
	mail VARCHAR(255) PRIMARY KEY NOT NULL,
	passwd VARCHAR(255) NOT NULL,
	nom VARCHAR(32),
	prenom VARCHAR(32),
	adresse VARCHAR(255)
);

CREATE TABLE Produits
(
	idProduit INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	libProduit VARCHAR(255),
	prix DECIMAL(5,2),
	couleur VARCHAR(16),
	quantiteStock INT,
	idMarque INT
);

CREATE TABLE Commandes
(
	idCommande INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	dateCommande DATE,
	adresse VARCHAR(255),
	statut VARCHAR(32),
	mail VARCHAR(255)
);

CREATE TABLE Marques
(
	idMarque INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	libMarque VARCHAR(255),
	urlMarque VARCHAR(255)
);

CREATE TABLE Images
(
	idImage INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	url VARCHAR(255),
	idProduit INT
);

CREATE TABLE Contenu_CMD
(
	idCommande INT,
	idProduit INT,
	quantite INT,
	PRIMARY KEY(idCommande, idProduit)
);

ALTER TABLE Produits ADD CONSTRAINT idMarque_table_Produits FOREIGN KEY (idMarque) REFERENCES Marques(idMarque);
ALTER TABLE Commandes ADD CONSTRAINT mail_table_Commandes FOREIGN KEY (mail) REFERENCES Clients(mail);
ALTER TABLE Images ADD CONSTRAINT idProduit_table_Images FOREIGN KEY (idProduit) REFERENCES Produits(idProduit);
ALTER TABLE Contenu_CMD ADD CONSTRAINT idCommande_table_Contenu_CMD FOREIGN KEY (idCommande) REFERENCES Commandes(idCommande);
ALTER TABLE Contenu_CMD ADD CONSTRAINT idProduit_table_Contenu_CMD FOREIGN KEY (idProduit) REFERENCES Produits(idProduit);

INSERT INTO Marques (libMarque, urlMarque) VALUES ('Nike', 'view/marques/img/nike_logo.png');
INSERT INTO Marques (libMarque, urlMarque) VALUES ('Adidas', 'view/marques/img/adidas_logo.png');
INSERT INTO Marques (libMarque, urlMarque) VALUES ('Puma', 'view/marques/img/puma_logo.png');

INSERT INTO Produits (libProduit, prix, couleur, quantiteStock, idMarque)
VALUES ('Metal Swoosh H86', 17, 'Blanc', 50, 1);
INSERT INTO Produits (libProduit, prix, couleur, quantiteStock, idMarque)
VALUES ('Essential Swoosh H86', 20, 'Obsidienne', 20, 1);
INSERT INTO Produits (libProduit, prix, couleur, quantiteStock, idMarque)
VALUES ('Trefoil Classic', 18, 'Rouge', 50, 2);
INSERT INTO Produits (libProduit, prix, couleur, quantiteStock, idMarque)
VALUES ('Climacool Running', 25, 'Ambre', 20, 2);
INSERT INTO Produits (libProduit, prix, couleur, quantiteStock, idMarque)
VALUES ('Climacool Running', 25, 'Noir', 10, 3);
INSERT INTO Produits (libProduit, prix, couleur, quantiteStock, idMarque)
VALUES ('Archive Logo Baseball', 18, 'Noir', 50, 3);

INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/nike_cap1_1.png', 1);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/nike_cap1_2.png', 1);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/nike_cap2_1.png', 2);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/nike_cap2_2.png', 2);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/adidas_cap3_1.png', 3);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/adidas_cap3_2.png', 3);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/adidas_cap1_1.png', 4);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/adidas_cap1_2.png', 4);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/adidas_cap2_1.png', 5);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/adidas_cap2_2.png', 5);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/puma_cap1_1.png', 6);
INSERT INTO Images (url, idProduit)
VALUES ('view/produits/img/puma_cap1_2.png', 6);