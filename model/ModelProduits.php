<?php
    require_once(File::build_path(array('model', 'Model.php')));

    class ModelProduits extends Model {
        private $idProduit;
        private $libProduit;
        private $prix;
        private $couleur;
        private $quantiteStock;
        private $idMarque;
        protected static $object = 'Produits';
        protected static $primary = 'idProduit';

        public function __construc($id = NULL, $lib = NULL, $prix = NULL, $couleur = NULL, $quantiteStock = NULL, $idMarque = NULL) {
            if(!is_null($id) && !is_null($lib) && !is_null($prix) && !is_null($couleur) && !is_null($quantiteStock) && !is_null($idMarque)) {
                $this->idProduit = $id;
                $this->libProduit = $lib;
                $this->prix = $prix;
                $this->couleur = $couleur;
                $this->quantiteStock = $quantiteStock;
                $this->idMarque = $idMarque;
            }
        }

        public function __set($property, $value)
        {
          $this->$property = $value;
        }

        public function __get($property)
        {
          return $this->$property;
        }


        public function getMarque() {
            $requete = "SELECT libMarque FROM Marques WHERE idMarque = ".$this->idMarque.";";
            $rep = Model::$pdo->query($requete);
            return $rep->fetchColumn();
        }

        public function getURLImages() {
            $requete = "SELECT url FROM Images WHERE idProduit = ".$this->idProduit.";";
            $rep = Model::$pdo->query($requete);
            $rep->setFetchMode(PDO::FETCH_NUM);        
            return $rep;
        }

        public static function getAllProduits() {
            return ModelProduits::selectAll();
        }

        public static function getProduitById($id) {
            return ModelProduits::select($id);
        }
    }
?>