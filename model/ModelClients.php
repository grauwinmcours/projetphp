<?php
    require_once(File::build_path(array('model', 'Model.php')));
    require_once(File::build_path(array('model', 'ModelCommandes.php')));
    require_once(File::build_path(array('lib', 'Security.php')));

    class ModelClients extends Model {
        private $mail;
        private $passwd;
        private $nom;
        private $prenom;
        private $adresse;
        protected static $object = 'Clients';
        protected static $primary = 'mail';

        public function __construct($m = NULL, $pa = NULL, $n = NULL, $pr = NULL, $a = NULL) {
            if(!is_null($m) && !is_null($pa) && !is_null($n) && !is_null($pr)) {
                $this->mail = $m;
                $this->passwd = $pa;
                $this->nom = $n;
                $this->prenom = $pr;
                $this->adresse = $a;

            }
        }

        public function __set($property, $value)
        {
          $this->$property = $value;
        }

        public function __get($property)
        {
          return $this->$property;
        }

        public static function getAllClients() {
            return ModelClients::selectAll();
        }

        public static function getClientByMail($mail) {
            return ModelClients::select($mail);
        }

        public function saveClient() {
            if(ModelClients::getClientByMail($this->mail) == FALSE) {
                $requete = "INSERT INTO Clients VALUES (:mail_tag, :passwd_tag, :nom_tag, :prenom_tag, :adresse_tag);";
                $prep_req = Model::$pdo->prepare($requete);
                $values = array(
                    "mail_tag" => $this->mail,
                    "passwd_tag" => Security::chiffrer($this->passwd),
                    "nom_tag" => $this->nom,
                    "prenom_tag" => $this->prenom,
                    "adresse_tag" => $this->adresse
                );
                return $prep_req->execute($values);
            } else {
                return false;
            }
        }

        public function updateClient($data) {
            $requete = "UPDATE Clients SET nom =:nom_tag, prenom =:prenom_tag, adresse =:adresse_tag WHERE mail =:mail_tag ;";
            $prep_req = Model::$pdo->prepare($requete);
            $values = array(
                "mail_tag" => $data['mail'],
                "nom_tag" => $data['nom'],
                "prenom_tag" => $data['prenom'],
                "adresse_tag" => $data['adresse']
            );
            return $prep_req->execute($values);
        }

        public static function checkPassword($mail, $passwd) {
            if(($client = ModelClients::getClientByMail($mail)) == FALSE) {
                return FALSE;
            }
            if(Security::chiffrer($passwd) == $client->passwd) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function getAllCommandes() {
            $tabCommandes = ModelCommandes::getAllCommandesOfClient($this->mail);
            if($tabCommandes == FALSE) return FALSE;
            else return $tabCommandes;
        }
    }
?>