<?php
    require_once(File::build_path(array('config', 'Config.php')));

    class Model {
        
        public static $pdo;

        public static function Init() {
            $login = Config::getLogin();
            $hostname = Config::getHostName();
            $database = Config::getDatabase();
            $password = Config::getPassword();

            try {
                self::$pdo = new PDO("mysql:host=$hostname;dbname=$database", $login, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")); //connexion BD
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //active l'affichage des erreurs
            } catch(PDOException $e) {
                if(Config::getDebug()) {
                    echo $e->getMessage();
                } else {
                    echo 'Une erreur est survenue (Model.php)...';
                }
                die();
            }
        }


        public static function selectAll() {
            $tableName = static::$object;
            $className = 'Model'.$tableName;
            $requete = "SELECT * FROM ".$tableName.";";
            try {
                $rep = self::$pdo->query($requete);
                $rep->setFetchMode(PDO::FETCH_CLASS, $className);
                return $rep;
            } catch(PDOException $e) {
                if(Config::getDebug()) {
                    echo $e->getMessage();
                } else {
                    return FALSE;
                }
            }
        }

        public static function select($primaryvalue) {
            $tableName = static::$object;
            $className = 'Model'.$tableName;
            $primaryKey = static::$primary;
            $requete = "SELECT * FROM ".$tableName." WHERE ".$primaryKey." = '".$primaryvalue."';";
            try {
                $rep = Model::$pdo->query($requete);
                $rep->setFetchMode(PDO::FETCH_CLASS, $className);
                $tab = $rep->fetchAll();
                if(empty($tab)) return FALSE;
                else return $tab[0];
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }

        public static function delete($primaryvalue) {
            $tableName = static::$object;
            $className = 'Model'.$tableName;
            $primaryKey = static::$primary;
            $requete = "DELETE FROM ".$tableName." WHERE ".$primaryKey." = '".$primaryvalue."';";
            try {
                Model::$pdo->query($requete);  
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
                     
        }
    }
    Model::Init();
?>