<?php
    require_once(File::build_path(array('model', 'Model.php')));

    class ModelImages extends Model {
        private $idImage;
        private $url;
        private $idProduit;

        public function __construct($idI = NULL, $url = NULL, $idP = NULL) {
            if(!is_null($idI) && !is_null($url) && !is_null($idP)) {
                $this->idImage = $idI;
                $this->url = $url;
                $this->idProduit = $idP;
            }
        }

        public function __set($property, $value)
        {
          $this->$property = $value;
        }

        public function __get($property)
        {
          return $this->$property;
        }
    }
?>