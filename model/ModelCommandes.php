<?php
    require_once(File::build_path(array('model', 'Model.php')));
    require_once(File::build_path(array('model', 'ModelClients.php')));

    class ModelCommandes extends Model {
        private $idCommande;
        private $dateCommande;
        private $adresse;
        private $statut;
        private $mail;
        protected static $object = 'Commandes';
        protected static $primary = 'idCommande';

        public function __construct($idC = NULL, $dateC = NULL, $adresse = NULL, $statut = NULL, $mail = NULL) {
            if(!is_null($idC) && !is_null($dateC) && !is_null($adresse) && !is_null($statut) && !is_null($mail)) {
                $this->idCommande = $idC;
                $this->dateCommande = $dateC;
                $this->adresse = $adresse;
                $this->statut = $statut;
                $this->mail = $mail;
            }
        }

        public function __set($property, $value)
        {
          $this->$property = $value;
        }

        public function __get($property)
        {
          return $this->$property;
        }

        public static function getContenuCMDById($idCommande) {
                $requete = "SELECT idProduit, quantite FROM Contenu_CMD WHERE idCommande = $idCommande;";
                $rep = Model::$pdo->query($requete);
                $rep->setFetchMode(PDO::FETCH_NUM);
                return $rep;
        }

        public static function getAllCommandesOfClient($mail) {
                $requete = "SELECT * FROM Commandes WHERE mail = '$mail';";
                $rep = Model::$pdo->query($requete);
                $rep->setFetchMode(PDO::FETCH_CLASS, 'ModelCommandes');
                if(empty($rep)) return FALSE;
                else return $rep;
        }

        public static function placeCommand() {
                if(!isset($_SESSION['mail'])) {
                        return 0;
                }
                $client = ModelClients::getClientByMail($_SESSION['mail']);
                $requete = "INSERT INTO Commandes (dateCommande, adresse, statut, mail) VALUES (:date_tag, :adresse_tag, :statut_tag, :mail_tag);";
                $prep_req = Model::$pdo->prepare($requete);
                $values = array(
                        "date_tag" => date('Y-m-d'),
                        "adresse_tag" => $client->__get("adresse"),
                        "statut_tag" => "En cours de préparation",
                        "mail_tag" => $_SESSION['mail']
                );
                $retour = $prep_req->execute($values);
                if($retour) {
                        $idCommande = "LAST_INSERT_ID()";
                        foreach($_SESSION['panier']['idProduit'] as $idProduit) {
                                $posProd = array_search($idProduit, $_SESSION['panier']['idProduit']);
                                $qteP = $_SESSION['panier']['quantite'][$posProd];
                                $requete_cmd = "INSERT INTO Contenu_CMD VALUES ($idCommande, $idProduit, $qteP);";
                                $rep = Model::$pdo->query($requete_cmd);
                        }
                }               
                unset($_SESSION['panier']);
                return $retour;
        }
    }