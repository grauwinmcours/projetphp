<?php
    require_once(File::build_path(array('model', 'Model.php')));

    class ModelMarques extends Model {
        private $idMarque;
        private $libMarque;
        private $urlMarque;
        protected static $object = 'Marques';
        protected static $primary = 'idMarque';

        public function __construct($id = NULL, $lib = NULL, $url = NULL) {
            if(!is_null($id) && !is_null($lib) && !is_null($url)) {
                $this->idMarque = $id;
                $this->libMarque = $lib;
                $this->urlMarque = $url;
            }
        }

        public function __set($property, $value)
        {
          $this->$property = $value;
        }

        public function __get($property)
        {
          return $this->$property;
        }

        public static function getAllMarques() {
            return ModelMarques::selectAll();
        }

        public static function getMarqueById($id) {
            return ModelMarques::select($id);
        }
    }
?>