<?php
    require_once(File::build_path(array('controller', 'ControllerClients.php')));
    require_once(File::build_path(array('controller', 'ControllerProduits.php')));
    require_once(File::build_path(array('controller', 'ControllerMarques.php')));
    require_once(File::build_path(array('controller', 'ControllerPanier.php')));
    require_once(File::build_path(array('controller', 'ControllerCommandes.php')));
    

    if(isset($_GET['controller'])) {
        $controller = $_GET['controller'];
        $controller_class = "Controller".ucfirst($controller);
        $class_exists = class_exists($controller_class);
        if(!$class_exists) {
            $action = "error";
            ControllerProduits::$action();
        } 
    } elseif (isset($_POST['controller'])) {
        $controller = $_POST['controller'];
        $controller_class = "Controller".ucfirst($controller);
        $class_exists = class_exists($controller_class);
        if(!$class_exists) {
            $action = "error";
            ControllerProduits::$action();
        } 
    } else {
        $controller_class = "ControllerProduits";
    }

    if(isset($_GET['action'])) {
        $action = $_GET['action'];
        $tab_methods_class = get_class_methods($controller_class);
        if(!in_array($action, $tab_methods_class)) {
            $action = "error";
        }
    } elseif(isset($_POST['controller'])) {
        $action = $_POST['action'];
        $tab_methods_class = get_class_methods($controller_class);
        if(!in_array($action, $tab_methods_class)) {
            $action = "error";
        }
    } else {
        $action = "readAll";
    }

    $controller_class::$action();
?>