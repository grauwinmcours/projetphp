<?php
    require_once(File::build_path(array('controller', 'ControllerProduits.php')));

    class ControllerPanier {

        public static function create() {
            if(!isset($_SESSION['panier'])) {
                $_SESSION['panier'] = array();
                $_SESSION['panier']['idProduit'] = array();
                $_SESSION['panier']['quantite'] = array();
            }
            return TRUE;
        }

        public static function add() {
            $idProduit = $_GET['idProduit'];

            if(ControllerPanier::create()) {
                $posProd = array_search($idProduit, $_SESSION['panier']['idProduit']);
                if($posProd !== FALSE ) {
                    $_SESSION['panier']['quantite'][$posProd] += 1;
                } else {
                    array_push($_SESSION['panier']['idProduit'], $idProduit);
                    array_push($_SESSION['panier']['quantite'], 1);
                }
                $controller = "panier"; $view = "added"; $pagetitle = "Mon panier";
                require_once(File::build_path(array('view', 'view.php')));   
                return TRUE;  
            } else {
                return FALSE;
            }
        }

        public static function deleteProduit() {
            $idProduit = $_GET['idProduit'];
            if(ControllerPanier::create()) {
                $tmp = array();
                $tmp['idProduit'] = array();
                $tmp['quantite'] = array();
                for($i=0; $i<count($_SESSION['panier']['idProduit']); $i++) {
                    if($_SESSION['panier']['idProduit'][$i] !== $idProduit) {
                        array_push($tmp['idProduit'], $_SESSION['panier']['idProduit'][$i]);
                        array_push($tmp['quantite'], $_SESSION['panier']['quantite'][$i]);
                    }
                }
                $_SESSION['panier'] = $tmp;
                unset($tmp);
                return TRUE;
            } else {
                return FALSE;
            }
        }


        public static function delete() {
            $idProduit = $_GET['idProduit'];
            if(ControllerPanier::create()) {
                $posProd = array_search($idProduit, $_SESSION['panier']['idProduit']);
                if($_SESSION['panier']['quantite'][$posProd] > 1) {
                    $_SESSION['panier']['quantite'][$posProd] -= 1;
                } else {
                    ControllerPanier::deleteProduit();
                }
                $controller = "panier"; $view = "deleted"; $pagetitle = "Mon panier";
                require_once(File::build_path(array('view', 'view.php')));
                return TRUE;
            } else {
                return FALSE;
            }          
        }

        public static function read() {            
            if(ControllerPanier::create()) {
                $controller = "panier"; $view = "detail"; $pagetitle = "Mon panier";
                require_once(File::build_path(array('view', 'view.php')));
            }
        }
    }
?>