<?php
    require_once(File::build_path(array('model', 'ModelClients.php')));

    class ControllerClients {

        public static function inscription() {
            $controller = "clients"; $view = "create"; $pagetitle = "S'inscrire";
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function created() {
            if(ModelClients::getClientByMail($_POST['mail']) != NULL) {
                $controller = "clients"; $view = "error"; $pagetitle = "Erreur Clients";
                $erreur = "Cet identifiant n'est pas disponible.";
                require_once(File::build_path(array('view', 'view.php')));
            }
            elseif($_POST['passwd'] != $_POST['confirm_passwd']) {
                $controller = "clients"; $view = "error"; $pagetitle = "Erreur Clients";
                $erreur = "Mot de passe différents";
                require_once(File::build_path(array('view', 'view.php')));
            }
            else {
                $client = new ModelClients($_POST['mail'], $_POST['passwd'], $_POST['nom'], $_POST['prenom'], $_POST['adresse']);
                if($client->saveClient()) {
                    $_SESSION['mail'] = $_POST['mail'];
                    $_SESSION['isAdmin'] = 0;
                    $controller = "clients"; $view = "created"; $pagetitle = "Félicitation !";
                    require_once(File::build_path(array('view', 'view.php')));
                } else {
                    $controller = "clients"; $view = "error"; $pagetitle = "Erreur Clients";
                    $erreur = "Une erreur s'est produite. Veuillez nous excuser";
                    require_once(File::build_path(array('view', 'view.php')));
                }
            }         
        }

        public static function connexion() {
            $controller = "clients"; $view = "connect"; $pagetitle = "Se connecter";
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function connected() {
            $mail = $_POST['mail'];
            $passwd = $_POST['passwd'];
            if(ModelClients::checkPassword($mail, $passwd)) {
                $_SESSION['mail'] = $mail;
                if($mail == "admin@admin.com") {
                    $_SESSION['isAdmin'] = 1;
                } else {
                    $_SESSION['isAdmin'] = 0;
                }
                $controller = "clients"; $view = "connected"; $pagetitle = "Connecté";
                require_once(File::build_path(array('view', 'view.php')));
            } else {
                $controller = "clients"; $view = "error"; $pagetitle = "Erreur Clients";
                $erreur = "Identifiant ou mot de passe incorrect";
                require_once(File::build_path(array('view', 'view.php')));
            }
        }

        public static function deconnexion() {
            $controller = "clients"; $view = "disconnect"; $pagetitle = "Deconnecté";
            session_unset();
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function read() { //lecture du profil de l'utilisateur (avec possibilité de modifier ses données)
            $controller = "clients"; $view = "detail"; $pagetitle = "Mon compte";
            $client = ModelClients::getClientByMail($_SESSION['mail']);
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function updated() {
            $client = ModelClients::getClientByMail($_GET['mail']);
            $client->__set("nom", $_GET['nom']);
            $client->__set("prenom", $_GET['prenom']);
            $client->__set("adresse", $_GET['adresse']);
            $data = [
                "mail" => $client->__get("mail"),
                "nom" => $client->__get("nom"),
                "prenom" => $client->__get("prenom"),
                "adresse" => $client->__get("adresse")
            ];
            if($client->updateClient($data) == TRUE) {
                $controller = "clients"; $view = "updated"; $pagetitle = "Mon compte";
                require_once(File::build_path(array('view', 'view.php')));
            } else {
                $controller = "clients"; $view = "error"; $pagetitle = "Erreur Clients";
                $erreur = "Une erreur s'est produite. Veuillez nous excuser";
                require_once(File::build_path(array('view', 'view.php')));                
            }
        }

        public static function readCommandes() {
            $client = ModelClients::getClientByMail($_SESSION['mail']);
            $tabCommandes = $client->getAllCommandes();
            if($tabCommandes == FALSE) {
                $controller = "clients"; $view = "error"; $pagetitle = "Erreur Clients";
                $erreur = "Une erreur s'est produite. Veuillez nous excuser";
                require_once(File::build_path(array('view', 'view.php')));  
            } else {
                $controller = "clients"; $view = "command"; $pagetitle = "Mes commandes";
                require_once(File::build_path(array('view', 'view.php')));
            }
        }

        /* Fonctions pour le controle du panier du client  => voir "ControllerPanier.php" */
    }