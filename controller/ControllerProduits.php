<?php
    require_once(File::build_path(array('model', 'ModelProduits.php')));

    class ControllerProduits {

        public static function readAll() {
            $tab_produits = ModelProduits::getAllProduits();
            $controller = "produits"; $view = "list"; $pagetitle = "Accueil";
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function read() {
            $produit = ModelProduits::getProduitById($_GET['idProduit']);
            $idProduit = $produit->__get("idProduit");
            $libProduit = $produit->__get("libProduit");
            $marque = $produit->getMarque();
            $tab = $produit->getURLImages();
            $prix = $produit->__get("prix");
            $couleur = $produit->__get("couleur");
            $stock = $produit->__get("quantiteStock");
            $controller = "produits"; $view = "detail"; $pagetitle = $libProduit;
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function error() {
            $controller = "produits"; $view = "error"; $pagetitle = "erreur";
            require_once(File::build_path(array('view', 'view.php'))); 
        }
    }
?>