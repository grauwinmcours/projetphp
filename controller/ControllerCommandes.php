<?php
    require_once(File::build_path(array('model', 'ModelCommandes.php')));
    require_once(File::build_path(array('model', 'ModelProduits.php')));

    class ControllerCommandes {

        public static function read() {
            $idCommande = $_GET['idCmd'];
            $tab_produits = ModelCommandes::getContenuCMDById($idCommande);
            $controller = "commandes"; $view = "detail"; $pagetitle = "Commande n°$idCommande";
            require_once(File::build_path(array('view', 'view.php')));
        }

        public static function order() {
            $valeur_retour = ModelCommandes::placeCommand();
            if($valeur_retour == 0) {
                $controller = "clients"; $view = "connect"; $pagetitle = "Se connecter";
                require_once(File::build_path(array('view', 'view.php')));                
            }
            elseif($valeur_retour == FALSE) {
                $controller = "commandes"; $view = "error"; $pagetitle = "Erreur commande";
                require_once(File::build_path(array('view', 'view.php')));
            } else {
                $controller = "commandes"; $view = "ordered"; $pagetitle = "Commande";
                require_once(File::build_path(array('view', 'view.php')));
            }
        }
    }
?>