<?php
    require_once(File::build_path(array('model', 'ModelMarques.php')));

    class ControllerMarques {

        public static function readAll() {
            $tab_marques = ModelMarques::getAllMarques();
            $controller = "marques"; $view = "list"; $pagetitle = "Nos Marques";
            require_once(File::build_path(array('view', 'view.php')));
        }
    }
?>