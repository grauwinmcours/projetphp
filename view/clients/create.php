<form method="post" action="./index.php">
  <fieldset>
    <legend>S'inscrire</legend>
    <input type='hidden' name='controller' value='clients'>
    <input type='hidden' name='action' value='created'>
    <p>
    	<label for="mail">E-mail</label> :
    	<input type="email" name="mail" id="mail_id" required/>
    </p>
    <p>
    	<label for="nom">Votre nom</label> :
    	<input type="text" name="nom" id="nom_id" required/>
    </p>
    <p>
    	<label for="prenom">Votre prénom</label> :
    	<input type="text" name="prenom" id="prenom_id" required/>
    </p>
    <p>
      <label for="adresse">Adresse</label> :
			<textarea name="adresse" rows=4 cols=40 required></textarea>  
    </p>
    <p>
      <label for="passwd">Mot de passe</label> :
      <input type="password" name="passwd" id="passwd_id" required/>
    </p>
    <p>
      <label for="confirm_passwd">Entrez le mot de passe à nouveau</label> :
      <input type="password" name="confirm_passwd" id="confirm_passwd_id" required/>
    </p>
    <p>
      <input type="submit" value="S'inscrire !" />
    </p>
  </fieldset> 
</form>