
<a href="index.php?controller=clients&action=readCommandes" title="Mes commandes"> <input type="button" value="Mes commandes"> </a>

<form method="get" action="./index.php">
	  <fieldset>
	    <legend>Mon compte</legend>
        <input type='hidden' name='controller' value='clients'>
	    <input type='hidden' name='action' value='updated'>
	    <p>
	    	<label for="mail">E-mail</label> :
	    	<input type="email" name="mail" value="<?php echo $client->__get("mail"); ?>" id="mail_id" readonly/>
	    </p>
	    <p>
	    	<label for="nom">Nom</label> :
	    	<input type="text" name="nom" value="<?php echo $client->__get("nom"); ?>" id="nom_id" required/>
	    </p>
	    <p>
	      <label for="prenom">Prénom</label> :
	      <input type="text" name="prenom" value="<?php echo $client->__get("prenom"); ?>" id="prenom_id" required/>
	    </p>
        <p>
				<?php if(isset($raison)) {echo "<p>$raison</p>";} ?>
	      <label for="adresse">Adresse</label> :
				<textarea name="adresse" rows=4 cols=40 required><?php echo $client->__get("adresse"); ?></textarea>   
	    </p>
	    <p>
	      <input type="submit" value="Mettre à jour" />
	    </p>
	  </fieldset> 
	</form>