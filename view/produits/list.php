<?php
    foreach($tab_produits as $p) {
        $tab = $p->getURLImages();
        $row = $tab->fetch();
        $libP = $p->__get("libProduit");
        $idP = $p->__get("idProduit");
        $couleurP = $p->__get("couleur");
        $prixP = $p->__get("prix");
        echo <<<END
        <div>
            <img src="$row[0]" alt="$libP" style ="max-width: 300px; height: auto;">
            <p><a href="./index.php?controller=produits&action=read&idProduit=$idP">$libP</a> | $couleurP | $prixP €</p>
        </div>
END;
    }
?>