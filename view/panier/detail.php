<?php
    if(count($_SESSION['panier']['idProduit']) == 0) {
        echo <<<END
        <div>
            <p>Votre panier est vide</p>
            <a href="index.php?controller=produits&action=readAll" title="Accueil"> <input type="button" value="Retour vers l'accueil"></a>
        </div>
END;
    } else {
        $prixPanier = 0;
        foreach($_SESSION['panier']['idProduit'] as $idProduit) {
            $produit = ModelProduits::getProduitById($idProduit);
            $posProd = array_search($idProduit, $_SESSION['panier']['idProduit']);
            $libP = $produit->__get("libProduit");
            $couleurP = $produit->__get("couleur");
            $marqueP = $produit->getMarque();
            $prixP = $produit->__get("prix");
            $qteP = $_SESSION['panier']['quantite'][$posProd];
            $prixPanier += $prixP * $qteP;
            echo <<<END
            <div>
                <p>$libP | $couleurP | $marqueP</p>
                <p>$prixP € | quantité dans le panier : $qteP | <a href="index.php?controller=panier&action=delete&idProduit=$idProduit" title="Supprimer un article"> <input type="button" value="Supprimer un article"> </a></p>
            </div>
END;
        }
        echo "<p> Montant du panier : $prixPanier €</p>";
        echo "<a href=\"index.php?controller=commandes&action=order\" title=\"Commander\"><input type=\"button\" value=\"Valider le panier\"></a>";
    }
?>