<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $pagetitle; ?></title>
    </head>
    <body>
    	<header>
			<ul id="menu">
			  <li><a href="index.php?controller=produits&action=readAll" title="Accueil">Accueil</a></li>
			  <li><a href="index.php?controller=marques&action=readAll" title="Marques">Marques</a></li>
			  <?php
			  if(!isset($_SESSION['mail'])) {
				  echo "<li><a href=\"index.php?controller=clients&action=inscription\" title=\"S'inscrire\">S'inscrire</a></li>";
				  echo "<li><a href=\"index.php?controller=clients&action=connexion\" title=\"Se connecter\">Se connecter</a></li>";
			  } else {
				  echo "<li><a href=\"index.php?controller=clients&action=read\" title=\"Mon compte\">Mon compte</a></li>";
				  echo "<li><a href=\"index.php?controller=clients&action=deconnexion\" title=\"Se déconnecter\">Se déconnecter</a></li>";
			  }
			  ?>
			  <li><a href="index.php?controller=panier&action=read" title="Mon panier">Mon panier</a></li>
			</ul>
    	</header>

		<?php
		require File::build_path(array("view", $controller, "$view.php"));
		?>

		<footer>
			<p>GRAUWIN Mathieu | BIGOT Florian | MARIE Florian</p>
		</footer>
    </body>
</html>