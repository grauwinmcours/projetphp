<?php
    foreach($tab_marques as $m) {
        $libM = $m->__get("libMarque");
        $urlM = $m->__get("urlMarque");
        echo <<<END
        <div>
            <p>$libM</p>
            <img src="$urlM" alt="$libM" style="max-width: 300px; height: auto;">
        </div>
END;
    }
?>