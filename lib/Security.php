<?php

    class Security {

        private static $seed = '64PptvWCqWx0w7mX';
        
        static public function getSeed() {
            return self::$seed;
        }

        static public function chiffrer($texte_en_clair) {
            $texte_chiffre = hash('sha256', Security::getSeed().$texte_en_clair);
            return $texte_chiffre;
        }
    }
?>