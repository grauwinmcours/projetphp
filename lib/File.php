<?php
	Class File {

		public static function build_path($path_array) {
			// $ROOT_FOLDER = /home/grauwin/Documents/ProjetPHP/projetphp/lib
			$DS = DIRECTORY_SEPARATOR;
			$ROOT_FOLDER = __DIR__ . $DS . "..";
			return $ROOT_FOLDER. '/' . join('/', $path_array);
		}
	}
?>